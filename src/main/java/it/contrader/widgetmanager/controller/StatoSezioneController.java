package it.contrader.widgetmanager.controller;

import com.querydsl.core.types.Predicate;
import it.contrader.generics.controller.PublicReadController;
import it.contrader.widgetmanager.domain.StatoSezione;
import it.contrader.widgetmanager.dto.StatoSezioneDTO;
import it.contrader.widgetmanager.repository.StatoSezioneRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JRolamo Code Generator
 */
@RestController
@CrossOrigin
@RequestMapping("/api/statosezione")
public class StatoSezioneController extends PublicReadController<StatoSezioneDTO> {

    @Override
    public Page<StatoSezioneDTO> getAll(
            @QuerydslPredicate(root = StatoSezione.class, bindings = StatoSezioneRepository.class) Predicate predicate,
            @RequestParam(defaultValue = "20") Integer pageSize,
            @RequestParam(defaultValue = "0") Integer pageNumber,
            @RequestParam(defaultValue = "ASC") Sort.Direction direction,
            @RequestParam(defaultValue = "id") String sortField) {
        return service.getAll(predicate, PageRequest.of(pageNumber, pageSize, direction, sortField));
    }

    @Override
    public Long count(Predicate predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
