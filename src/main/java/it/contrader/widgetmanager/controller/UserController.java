package it.contrader.widgetmanager.controller;

import com.querydsl.core.types.Predicate;
import io.swagger.annotations.Api;
import it.contrader.generics.controller.PublicReadController;
import it.contrader.widgetmanager.domain.User;
import it.contrader.widgetmanager.dto.UserDTO;
import it.contrader.widgetmanager.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JRolamo
 *
 * @since 1.0
 */
@CrossOrigin
@RestController
@RequestMapping("/api/user")
@Api(value = "Gestione User")
public class UserController extends PublicReadController<UserDTO> {

    @Override
    public Page<UserDTO> getAll(
            @QuerydslPredicate(root = User.class, bindings = UserRepository.class) Predicate predicate,
            @RequestParam(defaultValue = "20") Integer pageSize,
            @RequestParam(defaultValue = "0") Integer pageNumber,
            @RequestParam(defaultValue = "ASC") Direction direction,
            @RequestParam(defaultValue = "id") String sortField) {
        return service.getAll(predicate, PageRequest.of(pageNumber, pageSize, direction, sortField));
    }

    @Override
    public Long count(Predicate predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
