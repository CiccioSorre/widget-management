package it.contrader.widgetmanager.controller;

import com.querydsl.core.types.Predicate;
import it.contrader.generics.controller.ProtectedCrudController;
import it.contrader.widgetmanager.domain.Widget;
import it.contrader.widgetmanager.dto.WidgetDTO;
import it.contrader.widgetmanager.dto.request.NewWidgetRequest;
import it.contrader.widgetmanager.dto.request.PresaInCaricoWidgetRequest;
import it.contrader.widgetmanager.repository.WidgetRepository;
import it.contrader.widgetmanager.service.WidgetService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JRolamo Code Generator
 */
@RestController
@CrossOrigin
@RequestMapping("/api/widget")
public class WidgetController extends ProtectedCrudController<WidgetDTO> {

    @Autowired
    private WidgetService widgetService;

    @Override
    public Page<WidgetDTO> getAll(
            @QuerydslPredicate(root = Widget.class, bindings = WidgetRepository.class) Predicate predicate,
            @RequestParam(defaultValue = "20") Integer pageSize,
            @RequestParam(defaultValue = "0") Integer pageNumber,
            @RequestParam(defaultValue = "ASC") Sort.Direction direction,
            @RequestParam(defaultValue = "id") String sortField) {
        return service.getAll(predicate, PageRequest.of(pageNumber, pageSize, direction, sortField));
    }

    @GetMapping("/downloadScreenshotByWidgetId/{id}")
    public ResponseEntity<Resource> getScreenshotByWidgetId(@PathVariable("id") Long id) {
        byte[] binaryFile = service.read(id).getScreenshot();
        ByteArrayResource resource = new ByteArrayResource(binaryFile, "screenshot");
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + "screenshot")
                .body(resource);
    }

    @PostMapping("/newWidget")
    public WidgetDTO newWidget(@ModelAttribute NewWidgetRequest request) throws IOException {
        System.out.println("it.contrader.widgetmanager.controller.WidgetController.newWidget()" + request.toString());
        return widgetService.newWidget(request);
    }

    @PostMapping("/prendiWidgetInCarico")
    public WidgetDTO prendiWidgetInCarico(@RequestBody PresaInCaricoWidgetRequest request) throws Exception {
        return widgetService.prendiWidgetInCarico(request.getWidget(), request.getUsernameAssegnatario());
    }

    @PostMapping("/approvaGrafica")
    public WidgetDTO approvaGrafica(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.approvaGrafica(widget);
    }

    @PostMapping("/preapprovaGrafica")
    public WidgetDTO preApprovaGrafica(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.preApprovaGrafica(widget);
    }

    @PostMapping("/respingiGrafica")
    public WidgetDTO respingiGrafica(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.respingiGrafica(widget);
    }

    @PostMapping("/approvaFunzionale")
    public WidgetDTO approvaFunzionale(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.approvaFunzionale(widget);
    }

    @PostMapping("/preapprovaFunzionale")
    public WidgetDTO preApprovaFunzionale(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.preApprovaFunzionale(widget);
    }

    @PostMapping("/respingiFunzionale")
    public WidgetDTO respingiFunzionale(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.respingiFunzionale(widget);
    }

    @PostMapping("/approvaBackend")
    public WidgetDTO approvaBackend(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.approvaBackend(widget);
    }

    @PostMapping("/impostaInPendingStub")
    public WidgetDTO impostaInPendingStub(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.impostaInPendingStub(widget);
    }

    @PostMapping("/preapprovaBackend")
    public WidgetDTO preApprovaBackend(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.preApprovaBackend(widget);
    }

    @PostMapping("/respingiBackend")
    public WidgetDTO respingiBackend(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.respingiBackend(widget);
    }

    @PostMapping("/approvaQualita")
    public WidgetDTO approvaQualita(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.approvaQualita(widget);
    }

    @PostMapping("/respingiQualita")
    public WidgetDTO respingiQualita(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.respingiQualita(widget);
    }

    @PostMapping("/approvaFrontend")
    public WidgetDTO approvaFrontend(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.approvaFrontend(widget);
    }

    @PostMapping("/preapprovaFrontend")
    public WidgetDTO preApprovaFrontend(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.preApprovaFrontend(widget);
    }

    @PostMapping("/respingiFrontend")
    public WidgetDTO respingiFrontend(@RequestBody WidgetDTO widget) throws Exception {
        return widgetService.respingiFrontend(widget);
    }

    @Override
    public Long count(Predicate predicate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
