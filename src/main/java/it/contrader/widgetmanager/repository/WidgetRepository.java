package it.contrader.widgetmanager.repository;

import it.contrader.widgetmanager.domain.Widget;
import it.contrader.generics.repositoy.IRepository;
import java.util.Optional;
import javax.persistence.LockModeType;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

/**
 * @author JRolamo Code Generator
 */
@Repository
public interface WidgetRepository extends IRepository<Widget> {

    @Lock(LockModeType.PESSIMISTIC_READ)
    public Optional<Widget> findTopByOrderByCodiceDesc();
}
