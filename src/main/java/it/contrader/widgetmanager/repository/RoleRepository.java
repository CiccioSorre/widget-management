package it.contrader.widgetmanager.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.contrader.widgetmanager.domain.Role;
import it.contrader.generics.repositoy.IRepository;

/**
 * @author JRolamo
 * @since 1.0
 *
 */
@Repository
public interface RoleRepository extends IRepository<Role> {

	public List<Role> findByAuthority(String authority);
}
