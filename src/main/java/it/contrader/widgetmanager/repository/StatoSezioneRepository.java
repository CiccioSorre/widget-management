package it.contrader.widgetmanager.repository;

import org.springframework.stereotype.Repository;

import it.contrader.widgetmanager.domain.StatoSezione;
import it.contrader.generics.repositoy.IRepository;

/**
 * @author JRolamo Code Generator */
@Repository
public interface StatoSezioneRepository extends IRepository<StatoSezione> {

    public StatoSezione findByCodice(String codice);
    
}
