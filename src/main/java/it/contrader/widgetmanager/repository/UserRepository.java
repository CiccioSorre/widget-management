package it.contrader.widgetmanager.repository;

import org.springframework.stereotype.Repository;

import it.contrader.widgetmanager.domain.User;
import it.contrader.generics.repositoy.IRepository;

/**
 * @author JRolamo
 *
 * @since 1.0
 */
@Repository
public interface UserRepository extends IRepository<User> {

	public User findByUsername(String username);

}
