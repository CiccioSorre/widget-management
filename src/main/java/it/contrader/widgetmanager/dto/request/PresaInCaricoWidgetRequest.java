package it.contrader.widgetmanager.dto.request;

import it.contrader.widgetmanager.dto.WidgetDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Vittorio Valent
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PresaInCaricoWidgetRequest {

    private WidgetDTO widget;

    private String usernameAssegnatario;
}
