package it.contrader.widgetmanager.dto.request;

import it.contrader.widgetmanager.domain.enums.TipologiaWidgetEnum;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Vittorio Valent
 */
@Data
public class NewWidgetRequest {

    private String nome;

    private TipologiaWidgetEnum tipologia;

    private Float dimensioneX;

    private Float dimensioneY;

    private MultipartFile screenshot;
}
