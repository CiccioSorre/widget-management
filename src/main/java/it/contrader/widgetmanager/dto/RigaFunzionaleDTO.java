package it.contrader.widgetmanager.dto;

import it.contrader.generics.domain.AbstractDTO;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author JRolamo Code Generator
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(
        callSuper = false
)
public class RigaFunzionaleDTO extends AbstractDTO implements Serializable {

    private Long id;

    private Long idWidget;

    private String nome;

    private String tipo;

    private String inserimento;

    private String formato;

    private String visualizzato;

    private String note;
}
