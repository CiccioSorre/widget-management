package it.contrader.widgetmanager.dto;

import it.contrader.generics.domain.AuditDTO;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author JRolamo
 *
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserDTO extends AuditDTO implements UserDetails {

    private Long id;

    @NotEmpty(message = "{UserDTO.username.NotEmpty}")
    private String username;

    @NotEmpty(message = "{UserDTO.password.NotEmpty}")
    private String password;

    private List<RoleDTO> authorities;

    private boolean accountNonExpired = Boolean.TRUE;

    private boolean credentialsNonExpired = Boolean.TRUE;

    private boolean accountNonLocked = Boolean.TRUE;

    private boolean enabled = Boolean.TRUE;

    private String name;

    private String surname;

}
