package it.contrader.widgetmanager.dto;

import it.contrader.generics.domain.AbstractDTO;
import java.lang.Long;
import java.lang.String;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author JRolamo Code Generator */
@Data
@EqualsAndHashCode(
        callSuper = false
)
public class StatoSezioneDTO extends AbstractDTO {
    private Long id;

    private String codice;

    private String nome;

    private String descrizione;
}
