package it.contrader.widgetmanager.dto;

import it.contrader.generics.domain.AuditDTO;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author JRolamo Code Generator
 */
@Data
@EqualsAndHashCode(
        callSuper = false
)
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class WidgetDTO extends AuditDTO {

    private Long id;

    private String codice;

    private String tipologia;

    private UserDTO assegnatario;

    private String nome;

    private String noteNome;

    private Float dimensioneX;

    private Float dimensioneY;

    private String noteDimensioni;

    private String datiJson;

    private String noteDatiJson;

    private String apiMapping;

    private String actions;

    private String noteActions;

    private String datiInizializzazione;

    private String noteDatiInizializzazione;

    private byte[] screenshot;

    private String noteWidget;

    private List<RigaFunzionaleDTO> tabellaFunzionale;

    private StatoSezioneDTO statoGrafica;

    private StatoSezioneDTO statoFunzionale;

    private StatoSezioneDTO statoBackend;

    private StatoSezioneDTO statoQualita;

    private StatoSezioneDTO statoFrontend;

    private String noteGrafica;

    private String noteFunzionale;

    private String noteBackend;

    private String noteQualita;

    private String noteFrontend;
}
