package it.contrader.widgetmanager.dto;

import org.springframework.security.core.GrantedAuthority;

import it.contrader.generics.domain.AbstractDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author JRolamo
 *
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoleDTO extends AbstractDTO implements GrantedAuthority {

	private Long id;

	private String authority;
}
