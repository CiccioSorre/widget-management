package it.contrader.widgetmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Starter class for application
 *
 * @author JRolamo
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = "it.contrader")
public class WidgetManager {

    public static void main(String[] args) {
        SpringApplication.run(WidgetManager.class, args);
    }

}
