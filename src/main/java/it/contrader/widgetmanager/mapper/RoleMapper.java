package it.contrader.widgetmanager.mapper;

import it.contrader.generics.mapper.IMapper;
import it.contrader.widgetmanager.domain.Role;
import it.contrader.widgetmanager.dto.RoleDTO;
import org.mapstruct.Mapper;

/**
 * @author JRolamo
 * @since 1.0
 *
 */
@Mapper(componentModel = "spring")
public interface RoleMapper extends IMapper<Role, RoleDTO> {

}
