package it.contrader.widgetmanager.mapper;

import it.contrader.widgetmanager.domain.StatoSezione;
import it.contrader.widgetmanager.dto.StatoSezioneDTO;
import it.contrader.generics.mapper.IMapper;
import org.mapstruct.Mapper;

/**
 * @author JRolamo Code Generator */
@Mapper(
        componentModel = "spring"
)
public interface StatoSezioneMapper extends IMapper<StatoSezione, StatoSezioneDTO> {
}
