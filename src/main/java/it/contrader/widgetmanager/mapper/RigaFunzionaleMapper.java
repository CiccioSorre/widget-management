package it.contrader.widgetmanager.mapper;

import it.contrader.generics.mapper.IMapper;
import it.contrader.widgetmanager.domain.RigaFunzionale;
import it.contrader.widgetmanager.dto.RigaFunzionaleDTO;
import org.mapstruct.Mapper;

/**
 * @author JRolamo
 * @since 1.0
 *
 */
@Mapper(componentModel = "spring")
public interface RigaFunzionaleMapper extends IMapper<RigaFunzionale, RigaFunzionaleDTO> {

}
