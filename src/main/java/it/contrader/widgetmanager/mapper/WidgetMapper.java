package it.contrader.widgetmanager.mapper;

import it.contrader.widgetmanager.domain.Widget;
import it.contrader.widgetmanager.dto.WidgetDTO;
import it.contrader.generics.mapper.IMapper;
import org.mapstruct.Mapper;

/**
 * @author JRolamo Code Generator */
@Mapper(
        componentModel = "spring"
)
public interface WidgetMapper extends IMapper<Widget, WidgetDTO> {
}
