package it.contrader.widgetmanager.mapper;

import it.contrader.generics.mapper.IMapper;
import it.contrader.widgetmanager.domain.User;
import it.contrader.widgetmanager.dto.UserDTO;
import org.mapstruct.Mapper;

/**
 *
 * @author JRolamo
 * @since 1.0
 */
@Mapper(componentModel = "spring")
public interface UserMapper extends IMapper<User, UserDTO> {

}
