package it.contrader.widgetmanager.service;

import org.springframework.stereotype.Service;

import it.contrader.widgetmanager.domain.StatoSezione;
import it.contrader.widgetmanager.domain.enums.StatoSezioneEnum;
import it.contrader.widgetmanager.dto.StatoSezioneDTO;
import it.contrader.widgetmanager.repository.StatoSezioneRepository;
import it.contrader.generics.service.PublicService;

/**
 * @author JRolamo Code Generator */
@Service
public class StatoSezioneService extends PublicService<StatoSezione, StatoSezioneDTO> {

    public StatoSezioneDTO getStatoSezioneByCodice(StatoSezioneEnum codice){
        return mapper.toDTO(((StatoSezioneRepository) repository).findByCodice(codice.toString()));
    }
}
