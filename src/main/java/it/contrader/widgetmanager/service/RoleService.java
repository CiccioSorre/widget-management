package it.contrader.widgetmanager.service;

import it.contrader.generics.service.ProtectedService;
import it.contrader.widgetmanager.domain.Role;
import it.contrader.widgetmanager.dto.RoleDTO;
import org.springframework.stereotype.Service;

/**
 * @author JRolamo
 *
 * @since 1.0
 */
@Service
public class RoleService extends ProtectedService<Role, RoleDTO> {

}
