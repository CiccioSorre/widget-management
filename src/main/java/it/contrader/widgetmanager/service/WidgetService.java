package it.contrader.widgetmanager.service;

import it.contrader.generics.service.ProtectedService;
import it.contrader.widgetmanager.domain.Widget;
import it.contrader.widgetmanager.domain.enums.StatoSezioneEnum;
import it.contrader.widgetmanager.dto.RigaFunzionaleDTO;
import it.contrader.widgetmanager.dto.StatoSezioneDTO;
import it.contrader.widgetmanager.dto.WidgetDTO;
import it.contrader.widgetmanager.dto.request.NewWidgetRequest;
import it.contrader.widgetmanager.repository.WidgetRepository;
import java.io.IOException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author JRolamo Code Generator
 */
@Service
public class WidgetService extends ProtectedService<Widget, WidgetDTO> {

    @Autowired
    private WidgetRepository widgetRepository;

    @Autowired
    private StatoSezioneService statoSezioneService;

    @Autowired
    private UserService userService;

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public WidgetDTO create(WidgetDTO widget) {
        for (RigaFunzionaleDTO riga : widget.getTabellaFunzionale()) {
            if (riga.getIdWidget() == null) {
                riga.setIdWidget(widget.getId());
            }
        }
        return super.create(widget);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_GRAFICO') or hasRole('ROLE_FUNZIONALE') or hasRole('ROLE_BACKENDSUPERVISOR') or hasRole('ROLE_FRONTSUPERVISOR') or hasRole('ROLE_CONTROLLOQUALITA')")
    public WidgetDTO update(WidgetDTO widget) {
        for (RigaFunzionaleDTO riga : widget.getTabellaFunzionale()) {
            if (riga.getIdWidget() == null) {
                riga.setIdWidget(widget.getId());
            }
        }
        return super.update(widget);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.REPEATABLE_READ)
    public WidgetDTO newWidget(NewWidgetRequest request) throws IOException {
        StatoSezioneDTO statoPending = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.PENDING);
        Optional<Widget> ultimoWidget = widgetRepository.findTopByOrderByCodiceDesc();
        String codiceSuccessivo;
        if (ultimoWidget.isPresent()) {
            Integer serialeCodice = Integer.parseInt(ultimoWidget.get().getCodice().substring(1));
            codiceSuccessivo = "#" + String.format("%04d", serialeCodice + 1);
        } else {
            codiceSuccessivo = "#0001";
        }
        WidgetDTO widget = WidgetDTO.builder()
                .codice(codiceSuccessivo)
                .nome(request.getNome())
                .dimensioneX(request.getDimensioneX())
                .dimensioneY(request.getDimensioneY())
                .screenshot(request.getScreenshot().getBytes())
                .tipologia(request.getTipologia().toString())
                .statoGrafica(statoPending)
                .statoFunzionale(statoPending)
                .statoBackend(statoPending)
                .statoQualita(statoPending)
                .statoFrontend(statoPending)
                .build();
        return super.create(widget);
    }

    @PreAuthorize("hasRole('ROLE_FRONTENDDEVELOPER')")
    public WidgetDTO prendiWidgetInCarico(WidgetDTO widget, String username) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoFrontend().getCodice().equals(StatoSezioneEnum.PREAPPROVED.toString())) {
            if (widget.getAssegnatario() == null) {
                widget.setAssegnatario(userService.loadUserByUsername(username));
            } else {
                throw new Exception("Il widget è già in carico ad un altro sviluppatore");
            }
            return super.update(widget);
        } else {
            throw new Exception("Per prendere in carico un widget è necessario che prima questo sia stato pre-approvato nel frontend");
        }
    }

    @PreAuthorize("hasRole('ROLE_GRAFICO')")
    public WidgetDTO preApprovaGrafica(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoGrafica().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiPreApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.PREAPPROVED);
        widget.setStatoGrafica(statoDiPreApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_FUNZIONALE')")
    public WidgetDTO preApprovaFunzionale(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoFunzionale().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiPreApprovazione = statoSezioneService
                .getStatoSezioneByCodice(StatoSezioneEnum.PREAPPROVED);
        widget.setStatoFunzionale(statoDiPreApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_BACKENDSUPERVISOR')")
    public WidgetDTO impostaInPendingStub(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoBackend().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiPreApprovazione = statoSezioneService
                .getStatoSezioneByCodice(StatoSezioneEnum.PENDINGSTUB);
        widget.setStatoBackend(statoDiPreApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_BACKENDSTUBDEVELOPER')")
    public WidgetDTO preApprovaBackend(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoBackend().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiPreApprovazione = statoSezioneService
                .getStatoSezioneByCodice(StatoSezioneEnum.PREAPPROVED);
        widget.setStatoBackend(statoDiPreApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_FRONTSUPERVISOR')")
    public WidgetDTO preApprovaFrontend(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoFrontend().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        if (!widget.getStatoBackend().getCodice().equals(StatoSezioneEnum.PREAPPROVED.toString())) {
            throw new Exception("Non è possibile pre-approvare il frontend se il backend non è stato pre-approvato");
        }
        StatoSezioneDTO statoDiPreApprovazione = statoSezioneService
                .getStatoSezioneByCodice(StatoSezioneEnum.PREAPPROVED);
        widget.setStatoFrontend(statoDiPreApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_CONTROLLOQUALITA')")
    public WidgetDTO approvaQualita(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getAssegnatario() == null) {
            throw new Exception("Non è possibile approvate la qualità di un widget che non è in carico a nessuno");
        }
        if (!widget.getStatoFrontend().getCodice().equals(StatoSezioneEnum.PREAPPROVED.toString())) {
            throw new Exception("Prima di approvare la qualità deve essere pre-approvato il frontend");
        }
        StatoSezioneDTO statoDiApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.APPROVED);
        widget.setStatoQualita(statoDiApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_GRAFICO')")
    public WidgetDTO approvaGrafica(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (!tutteLeSezioniPreApprovate(widget)) {
            throw new Exception("Tutte le altre sezioni devono essere state pre-approvate per procedere all'approvazione");
        } else if (!widget.getStatoQualita().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Prima di procedere all'approvazione della grafica deve essere approvata la qualità del codice");
        }
        StatoSezioneDTO statoDiApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.APPROVED);
        widget.setStatoGrafica(statoDiApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_FUNZIONALE')")
    public WidgetDTO approvaFunzionale(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (!widget.getStatoGrafica().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Prima di approvare il funzionale deve essere approvata la grafica");
        }
        if (!tutteLeSezioniPreApprovate(widget)) {
            throw new Exception("Tutte le altre sezioni devono essere state pre-approvate per procedere all'approvazione");
        }
        StatoSezioneDTO statoDiApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.APPROVED);
        widget.setStatoFunzionale(statoDiApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_BACKENDSUPERVISOR')")
    public WidgetDTO approvaBackend(WidgetDTO widget) throws Exception {
        if (!widget.getStatoFunzionale().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Prima di approvare il backend deve essere approvato il funzionale");
        }
        widget = super.read(widget.getId());
        if (!tutteLeSezioniPreApprovate(widget)) {
            throw new Exception("Tutte le altre sezioni devono essere state pre-approvate per procedere all'approvazione");
        }
        StatoSezioneDTO statoDiApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.APPROVED);
        widget.setStatoBackend(statoDiApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_FRONTSUPERVISOR')")
    public WidgetDTO approvaFrontend(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (!widget.getStatoQualita().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Prima di approvare la qualità deve essere approvato il backend");
        }
        if (!tutteLeSezioniPreApprovate(widget)) {
            throw new Exception("Tutte le altre sezioni devono essere state pre-approvate per procedere all'approvazione");
        }
        StatoSezioneDTO statoDiApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.APPROVED);
        widget.setStatoFrontend(statoDiApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_GRAFICO')")
    public WidgetDTO respingiGrafica(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoGrafica().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiNonApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.REJECTED);
        widget.setStatoGrafica(statoDiNonApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_FUNZIONALE')")
    public WidgetDTO respingiFunzionale(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoFunzionale().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiNonApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.REJECTED);
        widget.setStatoFunzionale(statoDiNonApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_BACKENDSUPERVISOR')")
    public WidgetDTO respingiBackend(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoBackend().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiNonApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.REJECTED);
        widget.setStatoBackend(statoDiNonApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_CONTROLLOQUALITA')")
    public WidgetDTO respingiQualita(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoQualita().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiNonApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.REJECTED);
        widget.setStatoQualita(statoDiNonApprovazione);
        widget = super.update(widget);
        return widget;
    }

    @PreAuthorize("hasRole('ROLE_FRONTSUPERVISOR')")
    public WidgetDTO respingiFrontend(WidgetDTO widget) throws Exception {
        widget = super.read(widget.getId());
        if (widget.getStatoFrontend().getCodice().equals(StatoSezioneEnum.APPROVED.toString())) {
            throw new Exception("Non è possibile modificare una sezione se è già stata approvata");
        }
        StatoSezioneDTO statoDiNonApprovazione = statoSezioneService.getStatoSezioneByCodice(StatoSezioneEnum.REJECTED);
        widget.setStatoFrontend(statoDiNonApprovazione);
        widget = super.update(widget);
        return widget;
    }

    private boolean tutteLeSezioniPreApprovate(WidgetDTO widget) {
        return (widget.getStatoBackend().getCodice().equals(StatoSezioneEnum.APPROVED.toString())
                || widget.getStatoBackend().getCodice().equals(StatoSezioneEnum.PREAPPROVED.toString()))
                && (widget.getStatoFrontend().getCodice().equals(StatoSezioneEnum.APPROVED.toString())
                || widget.getStatoFrontend().getCodice().equals(StatoSezioneEnum.PREAPPROVED.toString()))
                && (widget.getStatoFunzionale().getCodice().equals(StatoSezioneEnum.APPROVED.toString())
                || widget.getStatoFunzionale().getCodice().equals(StatoSezioneEnum.PREAPPROVED.toString()))
                && (widget.getStatoGrafica().getCodice().equals(StatoSezioneEnum.APPROVED.toString())
                || widget.getStatoGrafica().getCodice().equals(StatoSezioneEnum.PREAPPROVED.toString()));
    }

}
