package it.contrader.widgetmanager.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.contrader.widgetmanager.domain.User;
import it.contrader.widgetmanager.dto.UserDTO;
import it.contrader.widgetmanager.repository.UserRepository;
import it.contrader.generics.service.PrivateService;

/**
 * @author JRolamo
 *
 * @since 1.0
 */
@Service
public class UserService extends PrivateService<User, UserDTO> implements UserDetailsService{
    
    @Override
    @Transactional
    public UserDTO loadUserByUsername(String username) throws UsernameNotFoundException {
        return mapper.toDTO(((UserRepository) repository).findByUsername(username));
    }
}
