package it.contrader.widgetmanager.filter;

import it.contrader.generics.domain.EntitySpecification;
import it.contrader.widgetmanager.domain.Widget;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author JRolamo Code Generator
 */
@Setter
@Getter
@AllArgsConstructor
public class WidgetSpecification extends EntitySpecification<Widget> {

    WidgetSpecification(Widget filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<Widget> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
        List<Predicate> p = new ArrayList<>();
        if (filter.getId() != null) {
            p.add(cb.equal(r.get("id"), filter.getId()));
        }
        if (filter.getCodice() != null) {
            p.add(cb.equal(r.get("codice"), filter.getCodice()));
        }

        if (filter.getNome() != null) {
            p.add(cb.equal(r.get("nome"), filter.getNome()));
        }

        if (filter.getNoteNome() != null) {
            p.add(cb.equal(r.get("noteNome"), filter.getNoteNome()));
        }

        if (filter.getNoteDimensioni() != null) {
            p.add(cb.equal(r.get("noteDimensioni"), filter.getNoteDimensioni()));
        }

        if (filter.getDatiJson() != null) {
            p.add(cb.equal(r.get("datiJson"), filter.getDatiJson()));
        }

        if (filter.getNoteDatiJson() != null) {
            p.add(cb.equal(r.get("noteDatiJson"), filter.getNoteDatiJson()));
        }

        if (filter.getDatiInizializzazione() != null) {
            p.add(cb.equal(r.get("datiInizializzazione"), filter.getDatiInizializzazione()));
        }

        if (filter.getNoteDatiInizializzazione() != null) {
            p.add(cb.equal(r.get("noteDatiInizializzazione"), filter.getNoteDatiInizializzazione()));
        }

        if (filter.getScreenshot() != null) {
            p.add(cb.equal(r.get("screenshot"), filter.getScreenshot()));
        }

        if (filter.getNoteWidget() != null) {
            p.add(cb.equal(r.get("noteWidget"), filter.getNoteWidget()));
        }

        if (filter.getStatoGrafica() != null) {
            p.add(cb.equal(r.get("statoGrafica"), filter.getStatoGrafica()));
        }

        if (filter.getStatoFunzionale() != null) {
            p.add(cb.equal(r.get("statoFunzionale"), filter.getStatoFunzionale()));
        }

        if (filter.getStatoBackend() != null) {
            p.add(cb.equal(r.get("statoBackend"), filter.getStatoBackend()));
        }

        if (filter.getStatoQualita() != null) {
            p.add(cb.equal(r.get("statoQualita"), filter.getStatoQualita()));
        }

        if (filter.getStatoFrontend() != null) {
            p.add(cb.equal(r.get("statoFrontend"), filter.getStatoFrontend()));
        }
        return q.where(cb.and(p.toArray(new Predicate[0]))).distinct(true).getRestriction();
    }
}
