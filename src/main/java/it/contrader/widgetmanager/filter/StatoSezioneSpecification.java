package it.contrader.widgetmanager.filter;

import it.contrader.generics.domain.EntitySpecification;
import it.contrader.widgetmanager.domain.StatoSezione;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author JRolamo Code Generator
 */
@Setter
@Getter
@AllArgsConstructor
public class StatoSezioneSpecification extends EntitySpecification<StatoSezione> {

    StatoSezioneSpecification(StatoSezione filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<StatoSezione> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
        List<Predicate> p = new ArrayList<>();
        if (filter.getId() != null) {
            p.add(cb.equal(r.get("id"), filter.getId()));
        }
        if (filter.getCodice() != null) {
            p.add(cb.equal(r.get("codice"), filter.getCodice()));
        }

        if (filter.getNome() != null) {
            p.add(cb.equal(r.get("nome"), filter.getNome()));
        }

        if (filter.getDescrizione() != null) {
            p.add(cb.equal(r.get("descrizione"), filter.getDescrizione()));
        }
        return q.where(cb.and(p.toArray(new Predicate[0]))).distinct(true).getRestriction();
    }
}
