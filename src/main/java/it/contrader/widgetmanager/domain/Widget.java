package it.contrader.widgetmanager.domain;

import it.contrader.generics.domain.AuditModel;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author JRolamo Code Generator
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(
        callSuper = false
)
public class Widget extends AuditModel implements Serializable {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    private String codice;

    @ManyToOne
    @JoinColumn
    private User assegnatario;

    private String nome;

    private String tipologia;

    private String noteNome;

    private Float dimensioneX;

    private Float dimensioneY;

    private String noteDimensioni;

    private String apiMapping;

    private String actions;

    private String noteActions;

    private String datiJson;

    @OneToMany(mappedBy = "idWidget", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.ALL})
    private List<RigaFunzionale> tabellaFunzionale;

    private String noteDatiJson;

    private String datiInizializzazione;

    private String noteDatiInizializzazione;

    @Lob
    private byte[] screenshot;

    private String noteWidget;

    @ManyToOne
    @JoinColumn
    private StatoSezione statoGrafica;

    @ManyToOne
    @JoinColumn
    private StatoSezione statoFunzionale;

    @ManyToOne
    @JoinColumn
    private StatoSezione statoBackend;

    @ManyToOne
    @JoinColumn
    private StatoSezione statoQualita;

    @ManyToOne
    @JoinColumn
    private StatoSezione statoFrontend;

    private String noteGrafica;

    private String noteFunzionale;

    private String noteBackend;

    private String noteQualita;

    private String noteFrontend;
}
