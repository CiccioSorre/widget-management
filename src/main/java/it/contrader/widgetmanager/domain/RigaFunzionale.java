package it.contrader.widgetmanager.domain;

import it.contrader.generics.domain.AbstractModel;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author JRolamo Code Generator
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(
        callSuper = false
)
public class RigaFunzionale extends AbstractModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "widget_id")
    private Long idWidget;

    private String nome;

    private String tipo;

    private String inserimento;

    private String formato;

    private String visualizzato;

    private String note;
}
