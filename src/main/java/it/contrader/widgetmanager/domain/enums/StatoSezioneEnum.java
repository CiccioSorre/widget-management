package it.contrader.widgetmanager.domain.enums;

public enum StatoSezioneEnum {
    PENDING,
    PENDINGSTUB,
    REJECTED,
    PREAPPROVED,
    APPROVED
}
