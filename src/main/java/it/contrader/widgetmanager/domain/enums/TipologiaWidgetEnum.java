package it.contrader.widgetmanager.domain.enums;

public enum TipologiaWidgetEnum {
    FORM,
    VISUALIZZAZIONE
}
