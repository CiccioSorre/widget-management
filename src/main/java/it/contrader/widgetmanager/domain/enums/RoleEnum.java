package it.contrader.widgetmanager.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RoleEnum {
    ADMIN("ROLE_ADMIN"),
    GRAFICO("ROLE_GRAFICO"),
    FUNZIONALE("ROLE_FUNZIONALE"),
    BACKENDSUPERVISOR("ROLE_BACKENDSUPERVISOR"),
    BACKENDSTUBDEVELOPER("ROLE_BACKENDSTUBDEVELOPER"),
    CONTROLLOQUALITA("ROLE_CONTROLLOQUALITA"),
    FRONTENDDEVELOPER("ROLE_FRONTENDDEVELOPER"),
    FRONTSUPERVISOR("ROLE_FRONTSUPERVISOR");

    private final String roleName;
}
