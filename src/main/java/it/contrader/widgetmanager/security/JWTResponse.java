package it.contrader.widgetmanager.security;

import java.util.List;

import it.contrader.widgetmanager.dto.RoleDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Container Class for JWT String
 * 
 * @author JRolamo
 *
 * @see JWTUtils
 * @see JWTRequest
 * @since 1.0
 */
@Data
@AllArgsConstructor
public class JWTResponse {

	private String token;

	private String username;

	private List<RoleDTO> authorities;
}
