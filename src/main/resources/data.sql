/**
 * Author:  Vittorio Valent
 */

INSERT INTO ROLE(AUTHORITY) VALUES
('ROLE_ADMIN'),
('ROLE_GRAFICO'),
('ROLE_FUNZIONALE'),
('ROLE_BACKENDSUPERVISOR'),
('ROLE_BACKENDSTUBDEVELOPER'),
('ROLE_FRONTENDDEVELOPER'),
('ROLE_CONTROLLOQUALITA'),
('ROLE_FRONTSUPERVISOR');

INSERT INTO `USER`(NAME, SURNAME, USERNAME, PASSWORD, OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON) VALUES
('String', 'String', 'grafico', '$2a$10$1yFOHygg9SDqog9Jr7EdqeNnnJ6xI1ycrS/Z1EECg47orIvhZLsx6', 'string', '2019-10-29 10:00:00', 'string', '2019-10-29 10:00:00');
INSERT INTO `USER_AUTHORITIES` VALUES (1,2);

INSERT INTO `USER`(NAME, SURNAME, USERNAME, PASSWORD, OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON) VALUES
('String', 'String', 'funzionale', '$2a$10$1yFOHygg9SDqog9Jr7EdqeNnnJ6xI1ycrS/Z1EECg47orIvhZLsx6', 'string', '2019-10-29 10:00:00', 'string', '2019-10-29 10:00:00');
INSERT INTO `USER_AUTHORITIES` VALUES (2,3);

INSERT INTO `USER`(NAME, SURNAME, USERNAME, PASSWORD, OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON) VALUES
('String', 'String', 'backend', '$2a$10$1yFOHygg9SDqog9Jr7EdqeNnnJ6xI1ycrS/Z1EECg47orIvhZLsx6', 'string', '2019-10-29 10:00:00', 'string', '2019-10-29 10:00:00');
INSERT INTO `USER_AUTHORITIES` VALUES (3,4);

INSERT INTO `USER`(NAME, SURNAME, USERNAME, PASSWORD, OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON) VALUES
('String', 'String', 'backendstub', '$2a$10$1yFOHygg9SDqog9Jr7EdqeNnnJ6xI1ycrS/Z1EECg47orIvhZLsx6', 'string', '2019-10-29 10:00:00', 'string', '2019-10-29 10:00:00');
INSERT INTO `USER_AUTHORITIES` VALUES (4,5);

INSERT INTO `USER`(NAME, SURNAME, USERNAME, PASSWORD, OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON) VALUES
('String', 'String', 'developer', '$2a$10$1yFOHygg9SDqog9Jr7EdqeNnnJ6xI1ycrS/Z1EECg47orIvhZLsx6', 'string', '2019-10-29 10:00:00', 'string', '2019-10-29 10:00:00');
INSERT INTO `USER_AUTHORITIES` VALUES (5,6);

INSERT INTO `USER`(NAME, SURNAME, USERNAME, PASSWORD, OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON) VALUES
('String', 'String', 'qualita', '$2a$10$1yFOHygg9SDqog9Jr7EdqeNnnJ6xI1ycrS/Z1EECg47orIvhZLsx6', 'string', '2019-10-29 10:00:00', 'string', '2019-10-29 10:00:00');
INSERT INTO `USER_AUTHORITIES` VALUES (6,7);

INSERT INTO `USER`(NAME, SURNAME, USERNAME, PASSWORD, OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON) VALUES
('String', 'String', 'frontend', '$2a$10$1yFOHygg9SDqog9Jr7EdqeNnnJ6xI1ycrS/Z1EECg47orIvhZLsx6', 'string', '2019-10-29 10:00:00', 'string', '2019-10-29 10:00:00');
INSERT INTO `USER_AUTHORITIES` VALUES (7,8);

INSERT INTO `USER`(NAME, SURNAME, USERNAME, PASSWORD, OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON) VALUES
('String', 'String', 'admin', '$2a$10$1yFOHygg9SDqog9Jr7EdqeNnnJ6xI1ycrS/Z1EECg47orIvhZLsx6', 'string', '2019-10-29 10:00:00', 'string', '2019-10-29 10:00:00');
INSERT INTO `USER_AUTHORITIES` VALUES (8,1);
INSERT INTO `USER_AUTHORITIES` VALUES (8,2);
INSERT INTO `USER_AUTHORITIES` VALUES (8,3);
INSERT INTO `USER_AUTHORITIES` VALUES (8,4);
INSERT INTO `USER_AUTHORITIES` VALUES (8,5);
INSERT INTO `USER_AUTHORITIES` VALUES (8,6);
INSERT INTO `USER_AUTHORITIES` VALUES (8,7);
INSERT INTO `USER_AUTHORITIES` VALUES (8,8);

INSERT INTO `STATO_SEZIONE` (CODICE, NOME, DESCRIZIONE) VALUES
('PENDING','Richiesto','Richiesto'),
('PENDINGSTUB','Richiesto in Stub','Richiesto in Stub'),
('PREAPPROVED','Pre-Approvato','Pre-Approvato'),
('APPROVED','Approvato','Approvato'),
('REJECTED','Non Approvato','Non Approvato');

INSERT INTO `WIDGET` (OWNER, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON,
    ACTIONS,
    CODICE,
    DATI_INIZIALIZZAZIONE,
    DATI_JSON,
    NOME,
    NOTE_ACTIONS,
    NOTE_BACKEND,
    NOTE_DATI_INIZIALIZZAZIONE,
    NOTE_DATI_JSON,
    NOTE_DIMENSIONI,
    NOTE_FRONTEND,
    NOTE_FUNZIONALE,
    NOTE_GRAFICA,
    NOTE_NOME,
    NOTE_QUALITA,
    NOTE_WIDGET,
    STATO_BACKEND_ID,
    STATO_FRONTEND_ID,
    STATO_FUNZIONALE_ID,
    STATO_GRAFICA_ID,
    STATO_QUALITA_ID
) VALUES ('admin', NOW(), 'admin', NOW(),
    'Cose varie di UNO',
    '#0001',
    'Dati Init UNO',
    'Dati UNO',
    'Widget UNO',
    'Cosa sono le Actions?!? UNO',
    'Non sparate ai backendisti UNO',
    'Dati di test... UNO!',
    'Dati JSON Non Coerenti UNO',
    'Dimensioni Casuali UNO',
    'Note Frontend UNO',
    'Note Funzionale UNO',
    'Note Grafica UNO',
    'Note Nome UNO',
    'Note Qualità UNO',
    'Note Widget UNO',
    1,
    1,
    1,
    1,
    1
),('admin', NOW(), 'admin', NOW(),
    'Cose varie di DUE',
    '#0002',
    'Dati Init DUE',
    'Dati DUE',
    'Widget DUE',
    'Cosa sono le Actions?!? DUE',
    'Non sparate ai backendisti DUE',
    'Dati di test... DUE!',
    'Dati JSON Non Coerenti DUE',
    'Dimensioni Casuali DUE',
    'Note Frontend DUE',
    'Note Funzionale DUE',
    'Note Grafica DUE',
    'Note Nome DUE',
    'Note Qualità DUE',
    'Note Widget DUE',
    1,
    1,
    1,
    1,
    1
),('admin', NOW(), 'admin', NOW(),
    'Cose varie di TRE',
    '#0003',
    'Dati Init TRE',
    'Dati TRE',
    'Widget TRE',
    'Cosa sono le Actions?!? TRE',
    'Non sparate ai backendisti TRE',
    'Dati di test... TRE!',
    'Dati JSON Non Coerenti TRE',
    'Dimensioni Casuali TRE',
    'Note Frontend TRE',
    'Note Funzionale TRE',
    'Note Grafica TRE',
    'Note Nome TRE',
    'Note Qualità TRE',
    'Note Widget TRE',
    1,
    1,
    1,
    1,
    1
),('admin', NOW(), 'admin', NOW(),
    'Cose varie di QUATTRO',
  	'#0004',
    'Dati Init QUATTRO',
    'Dati QUATTRO',
    'Widget QUATTRO',
    'Cosa sono le Actions?!? QUATTRO',
    'Non sparate ai backendisti QUATTRO',
    'Dati di test... QUATTRO!',
    'Dati JSON Non Coerenti QUATTRO',
    'Dimensioni Casuali QUATTRO',
    'Note Frontend QUATTRO',
    'Note Funzionale QUATTRO',
    'Note Grafica QUATTRO',
    'Note Nome QUATTRO',
    'Note Qualità QUATTRO',
    'Note Widget QUATTRO',
    1,
    1,
    1,
    1,
    1
),('admin', NOW(), 'admin', NOW(),
    'Cose varie di CINQUE',
    '#0005',
    'Dati Init CINQUE',
    'Dati CINQUE',
    'Widget CINQUE',
    'Cosa sono le Actions?!? CINQUE',
    'Non sparate ai backendisti CINQUE',
    'Dati di test... CINQUE!',
    'Dati JSON Non Coerenti CINQUE',
    'Dimensioni Casuali CINQUE',
    'Note Frontend CINQUE',
    'Note Funzionale CINQUE',
    'Note Grafica CINQUE',
    'Note Nome CINQUE',
    'Note Qualità CINQUE',
    'Note Widget CINQUE',
    1,
    1,
    1,
    1,
    1
);


INSERT INTO RIGA_FUNZIONALE (NOME, WIDGET_ID) VALUES ('TEST', 1);